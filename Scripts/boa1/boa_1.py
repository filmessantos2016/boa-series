#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
BàO 1: obtenir des données à partir d'un fichier RSS

1 - Lire un fichier RSS
2 - Extraction (récursive) de texte et de métadonnées

"""

import xml.etree.ElementTree as ET
import re
import feedparser
from typing import List, Dict
from pathlib import Path
import sys
from datetime import datetime

codes = {'une':'0,2-3208,1-0,0',
         'international':'0,2-3210,1-0,0',
         'europe':'0,2-3214,1-0,0',
         'societe':'0,2-3224,1-0,0',
         'idees':'0,2-3232,1-0,0',
         'economie':'0,2-3234,1-0,0',
         'actualite-medias':'0,2-3236,1-0,0',
         'sport':'0,2-3242,1-0,0',
         'planete':'0,2-3244,1-0,0',
         'culture':'0,2-3246,1-0,0',
         'livres':'0,2-3260,1-0,0',
         'cinema':'0,2-3476,1-0,0',
         'voyages':'0,2-3546,1-0,0',
         'technologies':'0,2-651865,1-0,0',
         'politique':'0,57-0,64-823353,0',
         'sciences':'env_sciences'

}



categories = { value: key for key, value in codes.items()}


def etree_parse_rss(path_file: str, tags:List[str] = ['title', 'description']) -> Dict[str, str]:
    """
    permettre la lecture d'un fichier rss avec l'utilisation de ElementTree
    retourner un dictionnaire avec les clés données en paramètre

    """
    tree = ET.parse(path_file)
    root = tree.getroot()

    for channel in root:
        for item in channel.findall('item'):
            yield {child.tag:child.text for child in item if child.tag in tags}

def clean_cdata(content: str) -> str:
    """
    permettre de nettoyer le contenu d'un tag CDATA
    """
    return re.sub(r'<!\[CDATA\[(.*?)\]\]>', r'\1', content)
    

def re_parse_rss(path_file: str, tags:List[str] = ['title', 'description']) -> Dict[str, str]:
    """
    permettre la lecture d'un fichier rss avec l'utilisation de regex
    retourner un dictionnaire avec les clés données en paramètre

    """
    
    with open(path_file, 'r') as f:
        content = f.read()
        #parcourir les items
        for item in re.findall(r'<item>(.*?)</item>', content, re.DOTALL):
            #parcourir les tags
            yield {tag:clean_cdata(re.search(r'<%s>(.*?)</%s>' % (tag, tag), item, re.DOTALL).group(1)) for tag in tags}

def feedparser_parse_rss(path_file: str, tags:List[str] = ['title', 'description']) -> Dict[str, str]:
    """
    permettre la lecture d'un fichier rss avec l'utilisation de feedparser
    retourner un dictionnaire avec les clés données en paramètre

    """
    feed = feedparser.parse(path_file)
    for entry in feed.entries:
        yield {tag:entry[tag] for tag in tags}


def browse_files(path: Path, patterns: any,start_date:datetime=None ,end_date:datetime=None, file_ext: str = '.xml') -> List[str]:
    """
    parcouris de manière récursive un répertoire et retourne la liste des fichiers avec une liste de patterns
    
    """
    #print("parcours de ", path)
    #test si le paramètre patterns est une liste
    if isinstance(patterns, list):
        patterns = '|'.join(patterns)
    regex = re.compile(patterns)

    for sub in sorted(path.iterdir()):
        #print(sub, sub.stem)
        if sub.is_dir():
            yield from browse_files(sub, patterns, file_ext, start_date, end_date)
        else:
            if sub.suffix == file_ext and regex.search(sub.name) :
                
                file_date = parse_date(str(sub).split('/')[1:4])

                if start_date is not None and file_date < start_date:
                    continue
                if end_date is not None and file_date > end_date:
                    continue
                
                yield sub, sub.stem


def parse_date(date_str: any) -> datetime:
    """
    parse une date au format str en date au format (année, mois, jour)
    exemple: parse_date(2021-01-01) ->  datetime(2021, 1, 1)
    
    
    """

    if isinstance(date_str, list):
        date_str = '-'.join(date_str)

    # remplacer '/' par '-'
    date_str = date_str.replace('/', '-')   

    try:
        # Essayer de parser la date avec le format "aaaa-mm-jj"
        date = datetime.strptime(date_str, "%Y-%m-%d")
        return date
    except ValueError:
        pass
    

    try:
        # Essayer de parser la date avec le format "aaaa-Mon-jj"
        date = datetime.strptime(date_str, "%Y-%b-%d")
        return date
    except ValueError:
        pass
    
    # Si aucun format n'a fonctionné, retourner une tuple vide
    return None
  




def extract_rss_to_xml(path_file: str, codes_categories:List[str], tags:List[str] = ['title', 'description'],start_date:datetime=None ,end_date:datetime=None, func_parser=etree_parse_rss) -> None:
    """
    permettre l'extraction des données d'un fichier rss vers un fichier xml
    """

    bheader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<corpus>\n"
    bfooter = "</corpus>"


    print(bheader)



    for in_rss,c in browse_files(Path(path_file), codes_categories, start_date, end_date):
            #print(">>fichier trouvé: ", in_rss)
        for item in func_parser(in_rss, tags):
                #print(">>item trouvé: ", item)
            print(f'\t<item categorie="{categories[c]}">')
            
            for tag, content in item.items():
                #print(tag, content)
                if tag in ['title', 'description']:
                    print(f'\t\t<{tag}><![CDATA[{content}]]></{tag}>')
                else:
                    print(f'\t\t<{tag}>{content}</{tag}>')
            
            print('\t</item>')

    print(bfooter)


parser_rss = {
    'etree': etree_parse_rss,
    're': re_parse_rss,
    'feedparser': feedparser_parse_rss
}




def main():

    path_file = 'Corpus/2022'

    date1 = datetime(2022, 1, 1)
    date2 = datetime(2022, 1, 2)

    funct_parser = parser_rss['etree']

    #test de la fonction browse_files et extract_rss_to_xml
    extract_rss_to_xml(path_file, codes_categories=['0,2-3208,1-0,0'], start_date=date1, end_date=date2,func_parser=funct_parser)
    
    sys.exit() 



if __name__ == '__main__':
    
    main()