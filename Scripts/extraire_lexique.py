#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import sys
import os
from pathlib import Path

def load_corpus(path_dir):
    """
    r1 : permettre la lecture du corpus comme une liste de fichiers en arguments
    python extraire_lexique.py Corpus/*.txt

    """
    corpus = []

     # Parcours tous les fichiers
    for file in path_dir:
        
        path = Path(file)

        # Vérifie si est un fichier
        if path.is_file():
            with path.open("r", encoding="utf-8") as f:
                corpus.append(f.read().strip())

        # Vérifie si le chemin correspond à un répertoire
        if path.is_dir():
             corpus = [fichier.read_text("utf-8").strip() for fichier in path.iterdir()]

    return corpus


def load_corpus_stdin():
    """
    r2 : permettre la lecture du corpus depuis l’entrée standard (un fichier par ligne, attention aux retours
    à la ligne dans les textes )
    cat Corpus/*.txt | python extraire_lexique.py

    r3 : permettre de lister les fichiers du corpus sur l’entrée standard
    ls Corpus/*.txt | python extraire_lexique.py
    """

    corpus = []
    

    # Parcourt les lignes de l'entrée standard
    for line in sys.stdin.readlines():
        
        # Supprime les espaces en début et fin de ligne
        line = line.strip()
        
        # Si la ligne n'est pas vide
        if line != '':
            
            # Vérifie si la ligne correspond à un fichier
            if os.path.isfile(line):
            # Si c'est le cas, on charge le contenu du fichier dans corpus
                with open(line,'r') as file:
                    corpus.append(file.read().strip())
            else:
                # Sinon, ajoute simplement la ligne au corpus
                corpus.append(line)  

    return corpus



def word_frequency(corpus):
    """
    Calcule la fréquence d'apparition de chaque terme dans le corpus donné

    """

    # Initialise un dictionnaire pour stocker la fréquence de chaque terme
    freq = {}
    # Parcourt chaque document dans le corpus
    for doc in corpus:
        # tokenization
        words = doc.split()
        # Parcourt chaque mot dans le document
        for word in words:
            # Incrémente la fréquence du mot s'il est déjà présent dans le dictionnaire
            if word in freq:
                freq[word] += 1
            # Ajoute le mot au dictionnaire et initialisation de la fréquence
            else:
                freq[word] = 1
    return freq


def doc_frequency(corpus):
    """
    Calcule la fréquence de chaque terme dans tous les documents

    """

    # Initialise un dictionnaire pour stocker le nombre de documents dans lesquels chaque terme apparaît
    freq = {}
    # Parcourt chaque document dans le corpus
    for doc in corpus:
        # tokenization et élimination des doublons 
        words = set(doc.split())
        # Parcourt chaque mot dans le document
        for word in words:
            # Incrémente la fréquence du mot s'il est déjà présent dans le dictionnaire
            if word in freq:
                freq[word] += 1
            # Ajoute le mot au dictionnaire et initialisation de la fréquence
            else:
                freq[word] = 1
    return freq
 


def main():
    
    parser = argparse.ArgumentParser(description="Extraction de lexique à partir d'un corpus de fichiers texte")
    parser.add_argument("files", nargs="*", help="Fichiers à traiter")    
    
    args = parser.parse_args()    

     # Si des données sont envoyées via l'entrée standard
    if not sys.stdin.isatty():
        corpus = load_corpus_stdin()

    elif args.files:
        # Charger les fichiers
        corpus = load_corpus(args.files)        

    else:
        # Si aucun fichier n'a été spécifié, afficher un message d'aide et terminer le script
        parser.print_help()
        sys.exit()    
    

    print("Fréquence des mots dans le corpus")    
    for word, freq in word_frequency(corpus).items():
        print(f"{word}: {freq}")


    print("Fréquence doc dans le corpus")    
    for word, freq in doc_frequency(corpus).items():
        print(f"{word}: {freq}")



if __name__ == "__main__":
    main()

