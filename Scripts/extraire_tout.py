#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import argparse
import sys,os
import re
from pathlib import Path
from boa1 import extract_rss_to_xml,codes,parse_date,parser_rss

def main():
    parser = argparse.ArgumentParser( description="Extraction des données d'un fichier rss vers format xml (stdout)" 
                                     , epilog="Exemple d'utilisation: python3 main.py -p Corpus/2022/Jun/ -c une|sports -t title|description|pubDate >output.xml" )
    parser.add_argument("-d",'--path' ,nargs='?',  default='Corpus/2022/', help="le fichier RSS ou le répertoire contenant les fichiers à traiter")  
    parser.add_argument("-c",'--categories' ,nargs='?',  help="la liste des categories à traiter, par défaut toutes les catégories sont traitées")  
    parser.add_argument('-t', '--tags', nargs='?', default='title|description|pubDate', help="balises rss à extraire par défaut : titre|description|pubDate")           
    parser.add_argument('-sd', '--start_date', nargs='?', default='2022-01-01', help="date de début de la période à extraire")
    parser.add_argument('-ed', '--end_date', nargs='?', default='2022-12-31', help="date de fin de la période à extraire")
    parser.add_argument('-p', '--parser', nargs='?', default='etree', help="méthode de parsing (re|etree|feedparser) par défaut etree")

    args = parser.parse_args()

    if args.tags:
         tags = re.split(r'[|,; ]', args.tags)
         #print(f'test paramètre tags: {tags}')

    
    codes_categories = []
    if args.categories:
        #split categories with '|,; ' and remove empty strings
        categories = re.split(r'[|,; ]', args.categories)
        
        for categorie in categories:
            if categorie not in codes:
                parser.error(f"categorie '{categorie}' non valide")
            else:
                codes_categories.append(codes[categorie])

    else:
        codes_categories = list(codes.values())
        #

    if args.path:
        
        path_files = args.path

        #test si le chemin existe
        if os.path.exists(path_files):
            #test s'il s'agit d'un répertoire
            path_files = Path(path_files)
           
        else:
            parser.error(f"le répertoire ou le fichier '{args.path}' n'existe pas")
    
    

    if args.start_date:
        start_date = parse_date(args.start_date)
        #print(f'test paramètre start_date: {start_date}')
    
    if args.end_date:
        end_date = parse_date(args.end_date)
        #print(f'test paramètre end_date: {end_date}')


    
    if args.parser not in parser_rss.keys():
        parser.error(f"parser '{args.parser}' non valide")
            
    parser_rss_m = parser_rss[args.parser]
    

    #print(f'test paramètre parser: {parser_rss_m}')




    extract_rss_to_xml(path_files, codes_categories, tags, start_date=start_date, end_date=end_date, func_parser=parser_rss_m)

    sys.exit() 



if __name__ == '__main__':
    
    main()