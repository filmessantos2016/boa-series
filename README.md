# BOA Series


# 2022/2023-L8TI003-P Program. et projet encadré 2

## Membres du groupe: 
Kenza AHMIA, Amina DJARFI

## Responsables du cours
Pierre Magistry / Marine Wauquier (contact : pierre.magistry@inalco.fr)


## Descriptif du cours

Mise en oeuvre d'une chaîne de traitement textuel semi-automatique, depuis la récupération des données jusqu'à leur présentation.
Ce cours posera d'abord la question des objectifs linguistiques à atteindre (lexicologie, recherche d'information, traduction...) et fera appel aux méthodes et outils informatiques nécessaires à leur réalisation (récupération de corpus, normalisation des textes, segmentation, étiquetage, extraction, structuration et présentation des résultats...).


